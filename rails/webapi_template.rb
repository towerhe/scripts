# Usage:
#
# $ APP_NAME=your_app_name APP_REPO_URL=your_app_repo_url foreman run rails new your_app_name -m webapi_template.rb -VSJT
#
RUBY_VERSION = ENV['RUBY_VERSION'] || '2.1.2'

unless ENV['APP_NAME']
  puts 'Your should specify a name of the application by a environment variable named APP_NAME.'
  exit
end

file '.ruby-version', <<-CODE
#{RUBY_VERSION}
CODE

# Configure Gemfile
add_source "http://ruby.taobao.org"

gem 'puma'

# Cross-Origin Resource Sharing (CORS)
gem 'rack-cors', :require => 'rack/cors'
gem 'ancestry'
gem 'seed-fu', '~> 2.3'

gem 'paper_trail', '~> 3.0.3'
gem 'rails-i18n'

# Setup envirnoment
file '.env', <<-CODE
APP_NAME=#{ENV['APP_NAME']}
APP_REPO_URL=#{ENV['APP_REPO_URL']}

ROLE_APP_SERVERS=deployer@42.42.62.73.23:22006
ROLE_DB_SERVERS=deployer@42.42.62.73.23:22006

APP_PUMA_BIND=tcp://0.0.0.0:9292

APP_SERVER=42.62.73.23:22006
APP_SERVER_SSH_KEYS=~/.ssh/id_rsa
CODE

environment 'config.time_zone = "Beijing"'
environment 'config.i18n.default_locale = "zh-CN"'
environment 'config.autoload_paths << Rails.root.join("lib")'
environment <<-CONFIG
config.middleware.use Rack::Cors do
      allow do
        origins '*'
        resource '*', :headers => :any, :methods => [:get, :post, :put, :delete]
      end
    end
CONFIG

# Setup railsapp-support
gem 'railsapp-support', git: 'git@gitlab.com:mlf-base/railsapp-support.git'

initializer 'railsapp_support.rb', <<-CODE
RailsappSupport.configure do |config|
  # 配置JSON API控制器的基类，默认值为ApplicationController
  # config.parent_controller_class = 'ApplicationController'

  # 启用用户管理api
  config.enable_users_api = true

  # 启用用户注册
  config.enable_registration = true

  config.authentication = :database
end

User.send :has_paper_trail, ignore: [:id, :encrypted_password, :created_at, :updated_at]
CODE

initializer 'devise.rb', <<-CODE
Devise.setup do |config|
  require 'devise/orm/active_record'

  config.authentication_keys = [ :login ]
  config.case_insensitive_keys = [ :login ]
  config.strip_whitespace_keys = [ :login ]
  config.skip_session_storage = [:http_auth]
  config.stretches = Rails.env.test? ? 1 : 10
  config.reconfirmable = true
  config.password_length = 8..128
  config.timeout_in = 30.minutes
  config.expire_auth_token_on_timeout = true
  config.lock_strategy = :none
  config.unlock_strategy = :none
  config.reset_password_within = 6.hours
  config.sign_out_via = :delete
end
CODE

# Setup authority
initializer 'authority.rb', <<-CODE
Authority.configure do |config|
  abilities = {
    create: 'creatable',
    read:   'readable',
    update: 'updatable',
    delete: 'deletable'

    # 定义新的abilities
  }.merge! RailsappSupport.authority_abilities
end
CODE

file 'app/authorizers/application_authorizer.rb', <<-CODE
# Other authorizers should subclass this one
class ApplicationAuthorizer < Authority::Authorizer

  # Any class method from Authority::Authorizer that isn't overridden
  # will call its authorizer's default method.
  #
  # @param [Symbol] adjective; example: `:creatable`
  # @param [Object] user - whatever represents the current user in your app
  # @return [Boolean]
  def self.default(adjective, user)
    # 'Whitelist' strategy for security: anything not explicitly allowed is
    # considered forbidden.
    false
  end

end
CODE

file 'app/authorizers/admin_authorizer.rb', <<-CODE
class AdminAuthorizer < ApplicationAuthorizer
  def self.readable_by?(user)
    user.has_role? :admins
  end

  def self.creatable_by?(user)
    user.has_role? :admins
  end

  def updatable_by?(user)
    user.has_role? :admins
  end

  def deletable_by?(user)
    user.has_role? :admins
  end
end
CODE

# Setup controllers
file 'app/controllers/application_controller.rb', <<-CODE
# -*- encoding: utf-8 -*-
class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  acts_as_token_authentication_handler_for User, fallback_to_devise: false

  before_filter :set_paper_trail_whodunnit, :set_paper_trail_controller_info

  def user_for_paper_trail
    signed_in? ? (current_user.realname || current_user.username) : '未知用户'
  end
end
CODE

file 'app/controllers/v1/base_controller.rb', <<-CODE
class V1::BaseController < ApplicationController
end
CODE

# Setup routes
file 'config/routes.rb', <<-CODE
Rails.application.routes.draw do
  devise_for :users, controllers: { sessions: 'sessions' }, path: "/v1/users"

  namespace :v1 do
    resources :users, controller: "/railsapp_support/json_api/users" do
      member do
        post :lock
        post :unlock
      end
    end
    resource :password, controller: "/railsapp_support/json_api/passwords", only: [:update]
  end
end
CODE

# Setup capistrano
gem_group :development do
  gem 'capistrano-rbenv'
  gem 'capistrano-rails'
  gem 'capistrano3-puma', github: "seuros/capistrano-puma"
end

file 'Capfile', <<-CODE
# Load DSL and Setup Up Stages
require 'capistrano/setup'

# Includes default deployment tasks
require 'capistrano/deploy'

# Includes tasks from other gems included in your Gemfile
#
# For documentation on these, see for example:
#
#   https://github.com/capistrano/rvm
#   https://github.com/capistrano/rbenv
#   https://github.com/capistrano/chruby
#   https://github.com/capistrano/bundler
#   https://github.com/capistrano/rails
#
# require 'capistrano/rvm'
# require 'capistrano/rbenv'
# require 'capistrano/chruby'
# require 'capistrano/bundler'
# require 'capistrano/rails/assets'
# require 'capistrano/rails/migrations'

require 'capistrano/rbenv'
require 'capistrano/bundler'
require 'capistrano/rails/migrations'
require 'capistrano/puma'
require 'capistrano/puma/nginx'

# Loads custom tasks from `lib/capistrano/tasks' if you have any defined.
Dir.glob('lib/capistrano/tasks/*.rake').each { |r| import r }
CODE

file 'config/deploy.rb', <<-CODE
# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, ENV['APP_NAME']
set :repo_url, ENV['APP_REPO_URL']

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, "/home/deployer/apps/rails/\#{fetch(:application)}"

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/secrets.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

set :rbenv_type, :user # or :system, depends on your rbenv setup
set :rbenv_ruby, '#{ENV["RUBY_VERSION"]}'
set :rbenv_prefix, "RBENV_ROOT=\#{fetch(:rbenv_path)} RBENV_VERSION=\#{fetch(:rbenv_ruby)} \#{fetch(:rbenv_path)}/bin/rbenv exec"
set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value

set :rails_env, 'production'                  # If the environment differs from the stage name

set :puma_init_active_record, true
set :puma_bind, ENV["APP_PUMA_BIND"].split(/;/)

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

require 'seed-fu/capistrano3'

# Trigger the task before publishing
before 'deploy:publishing', 'db:seed_fu'
CODE

file 'config/deploy/production.rb', <<-CODE
# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary server in each group
# is considered to be the first unless any hosts have the primary
# property set.  Don't declare `role :all`, it's a meta role.

role :app, ENV['ROLE_APP_SERVERS'].split(/;/)
role :db,  ENV['ROLE_DB_SERVERS'].split(/;/)

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server definition into the
# server list. The second argument is a, or duck-types, Hash and is
# used to set extended properties on the server.

server ENV['APP_SERVER'], user: 'deployer', roles: %w{app db}

# Custom SSH Options
# ==================
# You may pass any option but keep in mind that net/ssh understands a
# limited set of options, consult[net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start).
#
# Global options
# --------------
#  set :ssh_options, {
#    keys: %w(/home/rlisowski/.ssh/id_rsa),
#    forward_agent: false,
#    auth_methods: %w(password)
#  }
#
# And/or per server (overrides global)
# ------------------------------------
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }

if ENV['APP_SERVER_SSH_KEYS']
  set :ssh_options, {
    keys: ENV['APP_SERVER_SSH_KEYS'].split(/;/),
    forward_agent: false,
    auth_methods: %w(publickey password)
  }
end
CODE

file 'config/deploy/templates/nginx_conf.erb', <<-CODE
upstream puma_<%= fetch(:nginx_config_name) %> {
  server <%= fetch(:puma_bind) %> fail_timeout=0;
}

server {
  listen 80;

  client_max_body_size 4G;
  keepalive_timeout 10;

  error_page 500 502 504 /500.html;
  error_page 503 @503;

  server_name <%= fetch(:nginx_server_name) %>;
  root <%= current_path %>/public;
  try_files $uri/index.html $uri @puma_<%= fetch(:nginx_config_name) %>;

  location @puma_<%= fetch(:nginx_config_name) %> {
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header Host $http_host;
    proxy_redirect off;
    proxy_pass http://puma_<%= fetch(:nginx_config_name) %>;
    # limit_req zone=one;
    access_log <%= shared_path %>/log/nginx.access.log;
    error_log <%= shared_path %>/log/nginx.error.log;
  }

  location ^~ /assets/ {
    gzip_static on;
    expires max;
    add_header Cache-Control public;
  }

  location = /50x.html {
    root html;
  }

  location = /404.html {
    root html;
  }

  location @503 {
    error_page 405 = /system/maintenance.html;
    if (-f $document_root/system/maintenance.html) {
      rewrite ^(.*)$ /system/maintenance.html break;
    }
    rewrite ^(.*)$ /503.html break;
  }

  if ($request_method !~ ^(GET|HEAD|PUT|POST|DELETE|OPTIONS)$ ){
    return 405;
  }

  if (-f $document_root/system/maintenance.html) {
    return 503;
  }

  location ~ \.(php|html)$ {
    return 405;
  }
}
CODE

file 'config/deploy/templates/puma.rb.erb', <<-CODE
#!/usr/bin/env puma

directory '<%= current_path %>'
rackup "<%=fetch(:puma_rackup)%>"
environment '<%= fetch(:puma_env) %>'
<% if fetch(:puma_tag) %>
  tag '<%= fetch(:puma_tag)%>'
<% end %>
pidfile "<%=fetch(:puma_pid)%>"
state_path "<%=fetch(:puma_state)%>"
stdout_redirect '<%=fetch(:puma_access_log)%>', '<%=fetch(:puma_error_log)%>', true


threads <%=fetch(:puma_threads).join(',')%>

<%= puma_bind %>
workers <%= puma_workers %>
<% if fetch(:puma_worker_timeout) %>
worker_timeout <%= fetch(:puma_worker_timeout).to_i %>
<% end %>

<% if fetch(:puma_preload_app) %>
preload_app!
<% end %>

on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = "<%= fetch(:bundle_gemfile, "\#{current_path}/Gemfile") %>"
end

<% if fetch(:puma_init_active_record) %>
on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end
<% end %>
CODE

# Define default data which will be seed.
file 'db/fixtures/users.rb', <<-CODE
User.seed do |u|
  u.id = 1
  u.email = 'admin@gloit.cn'
  u.username = 'admin'
  u.realname = 'Administrator'
  u.password = '12345678'
  u.password_confirmation = '12345678'
end

User.find(1).add_role :admins
CODE

# Setup rspec
gem_group :development, :test do
  gem 'rspec-rails'
  gem 'factory_girl_rails'
  gem 'pry-rails'
end

gem_group :test do
  gem 'ffaker'
  gem 'json_expressions', require: 'json_expressions/rspec', github: 'chancancode/json_expressions'
end

file '.rspec', <<-CODE
 --color
--warnings
--require spec_helper
CODE

file 'spec/rails_helper.rb', <<-CODE
# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require 'spec_helper'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

# Checks for pending migrations before tests are run.
# If you are not using ActiveRecord, you can remove this line.
ActiveRecord::Migration.maintain_test_schema!

RSpec.configure do |config|
  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!
end
CODE

file 'spec/spec_helper.rb', <<-CODE
# This file was generated by the `rails generate rspec:install` command. Conventionally, all
# specs live under a `spec` directory, which RSpec adds to the `$LOAD_PATH`.
# The generated `.rspec` file contains `--require spec_helper` which will cause this
# file to always be loaded, without a need to explicitly require it in any files.
#
# Given that it is always loaded, you are encouraged to keep this file as
# light-weight as possible. Requiring heavyweight dependencies from this file
# will add to the boot time of your test suite on EVERY test run, even for an
# individual file that may not need all of that loaded. Instead, make a
# separate helper file that requires this one and then use it only in the specs
# that actually need it.
#
# The `.rspec` file also contains a few flags that are not defaults but that
# users commonly want.
#
# See http://rubydoc.info/gems/rspec-core/RSpec/Core/Configuration

require 'factory_girl'

RSpec.configure do |config|
# The settings below are suggested to provide a good initial experience
# with RSpec, but feel free to customize to your heart's content.
=begin
  # These two settings work together to allow you to limit a spec run
  # to individual examples or groups you care about by tagging them with
  # `:focus` metadata. When nothing is tagged with `:focus`, all examples
  # get run.
  config.filter_run :focus
  config.run_all_when_everything_filtered = true

  # Many RSpec users commonly either run the entire suite or an individual
  # file, and it's useful to allow more verbose output when running an
  # individual spec file.
  if config.files_to_run.one?
    # Use the documentation formatter for detailed output,
    # unless a formatter has already been configured
    # (e.g. via a command-line flag).
    config.default_formatter = 'doc'
  end

  # Print the 10 slowest examples and example groups at the
  # end of the spec run, to help surface which specs are running
  # particularly slow.
  config.profile_examples = 10

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = :random

  # Seed global randomization in this process using the `--seed` CLI option.
  # Setting this allows you to use `--seed` to deterministically reproduce
  # test failures related to randomization by passing the same `--seed` value
  # as the one that triggered the failure.
  Kernel.srand config.seed

  # rspec-expectations config goes here. You can use an alternate
  # assertion/expectation library such as wrong or the stdlib/minitest
  # assertions if you prefer.
  config.expect_with :rspec do |expectations|
    # Enable only the newer, non-monkey-patching expect syntax.
    # For more details, see:
    #   - http://myronmars.to/n/dev-blog/2012/06/rspecs-new-expectation-syntax
    expectations.syntax = :expect
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Enable only the newer, non-monkey-patching expect syntax.
    # For more details, see:
    #   - http://teaisaweso.me/blog/2013/05/27/rspecs-new-message-expectation-syntax/
    mocks.syntax = :expect

    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended.
    mocks.verify_partial_doubles = true
  end
=end

  config.include FactoryGirl::Syntax::Methods
end
CODE

file 'spec/factories/users.rb', <<-CODE
FactoryGirl.define do
  factory :user do
    username { Faker::Name.name }
    realname { Faker::Name.name }
    email { Faker::Internet.email username }
    password '12345678'
    password_confirmation '12345678'

    ignore do
      roles []
    end

    after :create do |user, evaluator|
      evaluator.roles.each { |r| user.add_role r }
    end

    factory :admin do
      roles [:admins]
    end
  end
end
CODE

# Add docs
file 'DEPLOYMENT.md', <<-CODE
## 部署说明书

1. 添加系统用户

  ```
  # 使用sudoers组用户账号登录服务器
  # 添加用户，并设置初始密码为20110816
  sudo adduser --gecos 'deployer' deployer
  ```

1. 配置服务器，用户deployer在部署机器上可以采用密钥建立与服务器的ssh连接，无须密码。

1. 配置服务器，可以采用密钥建立与git服务器的ssh连接，无须密码。

1. 安装环境，参见[rails-ubuntu-server-standalone.md](http://gitlab.com/mlf-routines/docs/blob/master/deploy/servers/rails-ubuntu-server-standalone.md)

1. 创建数据库（MySQL）

  ```
  # 使用sudoers组用户账号登录服务器
  # 登录到MySQL
  mysql -u root -p

  # 输入数据库密码

  # 创建生产环境下使用的数据库
  mysql> CREATE DATABASE IF NOT EXISTS `app_name` DEFAULT CHARACTER SET `utf8` COLLATE `utf8_unicode_ci`;

  # 为用户添加需要的表操作权限
  mysql> GRANT SELECT, LOCK TABLES, INSERT, UPDATE, DELETE, CREATE, DROP, INDEX, ALTER ON `app_name`.* TO 'deployer'@'localhost';

  # 退出数据库
  mysql> \q

  # 使用用户来连接到数据库
  sudo -u deployer -H mysql -u deployer -p -D app_name

  # 输入数据库用户deployer的密码

  # 那么应该可以看到'mysql>'提示符

  # 退出数据库
  mysql> \q
  ```

1. 创建数据库（PostgreSQL）

  ```
  # 使用sudoers组用户账号登录服务器
  # 登录到数据库
  sudo -u postgres psql -d template1

  # 创建生产环境需要使用的数据库，并配置权限
  template1=# CREATE DATABASE app_name OWNER deployer;

  # 退出数据库
  template1=# \q

  # 使用用户来连接数据库
  sudo -u deployer -H psql -d app_name
  ```

1. 检查部署的目录是否正确

  ```
  # 退出服务器
  # 在部署机上执行
  cd /path/to/your/app
  cap production deploy:check:directories
  ```

1. 配置数据库

  ```
  # 使用deployer登录服务器
  # 创建配置文件目录
  mkdir /home/deployer/apps/rails/app-name/shared/config

  # 配置数据库
  vim /home/deployer/apps/rails/app-name/shared/config/database.yml

  # 添加如下内容
  defaults: &defaults
    adapter: mysql2
    pool: 5
    username: deployer
    password: 20110816
    host: localhost
    port: 3306

  development:
    <<: *defaults
    database: app_name_development

  test:
    <<: *defaults
    database: app_name_test

  production:
    <<: *defaults
    database: app_name
  ```

1. 执行部署

  ```
  # 退出服务器
  # 在部署机上执行
  # 检查是否符合部署条件
  cap production deploy:check

  # 开始部署
  cap production deploy
  ```
CODE

file 'app/serializers/v1/.keep'

# Create README
run 'rm README.rdoc'
create_file "README.md", "# TODO"

# Clean up
run 'rm -fr app/assets'
run 'rm -fr app/helpers'
run 'rm -fr app/mailers'
run 'rm -fr app/views'
run 'rm -fr lib/assets'

# Run bundle install
run('bundle install')

# Setup paper_trail
generate 'paper_trail:install --with-changes'

# Setup DB
rake('railsapp_support:install:migrations')
rake('db:migrate')
rake('db:seed_fu')

# Git Setup
git :init

run('echo "/vendor" >> .gitignore')
run('echo ".env" >> .gitignore')

git add: "."
git commit: %Q{ -m "Initial commit" }